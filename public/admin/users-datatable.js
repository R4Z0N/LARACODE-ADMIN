var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#m_table_1").DataTable({
            responsive: true, 
            searchDelay: 500, 
            processing: true, 
            serverSide: true, 
            ajax:"/lc-admin/users", 
            columns: [
                { data: "name" }, 
                { data: "email" }, 
                { 
                    data: "roles", 
                    searchable: false,
                    orderable: false
                }, 
                { 
                    data: "permissions", 
                    searchable: false,
                    orderable: false
                },
                { data: "action" }
            ],
            columnDefs:[
            {
                targets: -1, 
                orderable: false,
                searchable: false,
                autoWidth: false,
                responsivePriority: -1,
            },
            {
                targets: 3,
                render: function (data) {
                    permissions = [];
                    $.each(data, function (i, permission) {
                        permissions.push(permission.name);
                    });

                    return permissions.join(', ');
                }
            }, 
            {
                targets: 2,
                title: "Roles",
                render: function (data) {
                    roles = [];
                    $.each(data, function (i, role) {
                        roles.push(role.name);
                    });

                    return roles.join(', ');
                }
            }]
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();
});
