<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeLanguage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:language {language}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates new language json file in resources/lang';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = base_path('resources/lang/' . $this->argument('language') . '.json');

        $file = fopen($path, 'w+');
        fwrite($file, '{'. PHP_EOL . PHP_EOL . '}');
        fclose($file);
    }
}
