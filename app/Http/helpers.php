<?php 

if (! function_exists('notify')) {	
	/**
	 * Helper for handling notifications
	 *
	 * @param $param1 string - type (string) or options (array)
	 * @param $param2 string - message (string) or settings (array)
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 * 
	 * @see http://bootstrap-notify.remabledesigns.com/
	 */	
	function notify($param1, $param2) {

		if (is_array($param1)) {
			$json = json_encode([
				$param1,
				$param2
			]);
		} 
		// use short syntax
		else {
			$json = json_encode([
				['message' => $param2],
				['type' => $param1]
			]);
		}

		$notification = str_replace(['[', ']'], ['', ''], $json);

		// redirect back
		return request()->ajax()
			? response()->json($json)
			: back()->withNotification($notification);
	}
}
