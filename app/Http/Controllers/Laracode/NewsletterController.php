<?php

namespace App\Http\Controllers\Laracode;

use App\Http\Controllers\Controller;
use App\Laracode\Newsletter;
use App\Mail\Laracode\NewsletterSignUp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class NewsletterController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|max:255',
        ]);

        $user = Newsletter::firstOrCreate([
                'email' => $request->email
            ],
            [
                'email' => $request->email,
                'user_id' => Auth::check() ? Auth::id() : null
            ]
        );

        // Mail::to($user->email)
        //     ->send(new NewsletterSignUp($user));

        return 200;
    }
}
