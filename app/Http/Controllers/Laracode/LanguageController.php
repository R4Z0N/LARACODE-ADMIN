<?php

namespace App\Http\Controllers\Laracode;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\URL;

class LanguageController extends Controller
{
    public function index()
    {
        return view('admin.languages');
    }

    public function store(Request $request)
    {
        if (in_array($request->name, config('app.admin_locales')))
            return notify('warning', 'Language already exists');

        Artisan::call('make-language', ['name' => $request->name]);

        return 201;
    }

    public function update(Request $request)
    {
        if (! in_array($request->language, config('app.admin_locales')))
            return notify('warning', 'Chosen language does not exist');

        Cookie::queue('admin-language', $request->language, 14400);
        
        return notify('success', 'The language has been changed');
    }

}
