<?php

// LC Admin Home
Breadcrumbs::for('lc-admin', function ($trail) {
    $trail->push('Home', route('lc-admin.dashboard'));
});

// LC Admin Activity Log
Breadcrumbs::for('lc-admin.activity-log', function ($trail) {
    $trail->parent('lc-admin');
    $trail->push('Activity log', route('lc-admin.activity-log'));
});

// LC Admin Roles
Breadcrumbs::for('lc-admin.roles', function ($trail) {
    $trail->parent('lc-admin');
    $trail->push('Roles', route('lc-admin.roles.index'));
});

// LC Admin Permissions
Breadcrumbs::for('lc-admin.permissions', function ($trail) {
    $trail->parent('lc-admin');
    $trail->push('Permissions', route('lc-admin.permissions.index'));
});

// LC Admin Home > Users
Breadcrumbs::for('lc-admin.users', function ($trail) {
    $trail->parent('lc-admin');
    $trail->push('Users', route('lc-admin.users.index'));
});

// LC Admin Home > Users > Create
Breadcrumbs::for('lc-admin.users.create', function ($trail) {
    $trail->parent('lc-admin.users');
    $trail->push('Create', route('lc-admin.users.create'));
});

// Home > Blog
Breadcrumbs::for('blog', function ($trail) {
    $trail->parent('home');
    $trail->push('Blog', route('blog'));
});

// Home > Blog > [Category]
Breadcrumbs::for('category', function ($trail, $category) {
    $trail->parent('blog');
    $trail->push($category->title, route('category', $category->id));
});

// Home > Blog > [Category] > [Post]
Breadcrumbs::for('post', function ($trail, $post) {
    $trail->parent('category', $post->category);
    $trail->push($post->title, route('post', $post->id));
});
