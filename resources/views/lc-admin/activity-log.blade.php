@extends('lc-admin.layouts.master')
@section('title', __('Activity Log'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<style>
    .shown+tr {
      background: #f8f9fa !important;
    }
    .shown+tr .datatable-old-value {
      color: red;
    }
    .shown+tr .datatable-new-value {
      color: green;
    }  
    div.slider {
      display: none;
    }
    table.dataTable tbody td.no-padding {
      padding: 0;
    }
</style>
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('Activity Log') }}</h3>
            
            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.activity-log') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-body-wrap-container">
                <div class="ks-full-table">
                    <div class="ks-full-table-header">
                        <h4 class="ks-full-table-name">{{ __('All activity')}}</h4>
                    </div>
                    <table id="table-activity-log" class="table ks-table-info dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th>{{ __('Description') }}</th>
                                <th>{{ __('Subject') }}</th>
                                <th>{{ __('Causer') }}</th>
                                <th>{{ __('Created at') }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('external-js')
    <script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/lc-admin/libs/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/activites-datatable.js') }}"></script>
@endsection
