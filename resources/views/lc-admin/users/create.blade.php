@extends('lc-admin.layouts.master')
@section('title', __('New User'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('New User') }}</h3>
            
            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.users.create') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('New User') }}</h5>
                                    <form action="{{ route('lc-admin.users.store') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="first-name-input" class="col-sm-3 form-control-label">{{ __('First name') }}</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="first-name" class="form-control" id="first-name-input" placeholder="{{ __('User first name') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="last-name-input" class="col-sm-3 form-control-label">{{ __('Last name') }}</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="last-name" class="form-control" id="last-name-input" placeholder="{{ __('User last name') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="contact-input" class="col-sm-3 form-control-label">{{ __('Contact') }}</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="contact" class="form-control" id="contact-input" placeholder="{{ __('User contact') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="email-input" class="col-sm-3 form-control-label">{{ __('Email') }}</label>
                                            <div class="col-sm-9">
                                                <input type="email" name="email" class="form-control" id="email-input" placeholder="{{ __('User email') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="password-input" class="col-sm-3 form-control-label">{{ __('Password') }}</label>
                                            <div class="col-sm-9">
                                                <input type="password" name="password" class="form-control" id="password-input" placeholder="{{ __('User password') }}">
                                            </div>
                                        </div>
                                        <button class="float-right btn btn-primary">{{ __('Register') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
