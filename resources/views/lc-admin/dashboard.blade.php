@extends('lc-admin.layouts.master')
@section('title', __('Dashboard'))
@section('content')
    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title">
                <h3>{{ __('Dashboard') }}</h3>
            </section>
        </div>
        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="container-fluid">
                    Blank page content.
                </div>
            </div>
        </div>
    </div>
@endsection